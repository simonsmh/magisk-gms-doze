#### GMS-Doze

Enable Doze for Google Play Service.

#### NOTICE

* You should use latest Magisk Manager to install this module. If you meet any problem under installation from Magisk Manager, please try to install it from recovery.
* Recent fixes:
Magisk v14 Template 1400 compatabilities

#### Credit & Support

* Copyright (C) 2017 simonsmh <simonsmh@gmail.com>
* Any issue or pull request is welcomed.
* Star this module at [GitHub](https://github.com/Magisk-Modules-Repo/magisk-gms-doze).
